function Hamburger(size, stuffing) {
    if(size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE){
        var burgerSize = size;
        this.getSize = function () {
            return burgerSize;
        };
    } else if (size === undefined) {
        throw new HamburgerException('Invalid size.');

    } else if (size !== Hamburger.SIZE_SMALL || Hamburger.SIZE_LARGE){
        throw new HamburgerException('No SIZE given.');
    }

    if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
        var burgerStuffing = stuffing;
        this.getStuffing = function () {

            return burgerStuffing
        };

    } else if (stuffing === undefined) {

        throw new HamburgerException('Invalid STAFFING.');
    } else if ( stuffing !== Hamburger.STUFFING_CHEESE || Hamburger.STUFFING_SALAD|| Hamburger.STUFFING_POTATO) {
        throw new HamburgerException('No STAFFING given.');
    }

    var burgerToppings = [];

    this.getBurgerToppings = function () {
        return burgerToppings;
    };
    this.setBurgerToppings = function (value) {
        return burgerToppings = value;
    };
}

// /* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10
};
Hamburger.ALL_TOPPINGS = {
    TOPPING_MAYO: {
        name: 'TOPPING_MAYO',
        price: 20,
        calories: 5
    },
    TOPPING_SPICE : {
        name: 'TOPPING_SPICE',
        price: 15,
        calories: 0
    }
};

Hamburger.prototype.addTopping = function (topping) {
    if (!this.getBurgerToppings().includes(topping)) {
        if (topping === Hamburger.ALL_TOPPINGS.TOPPING_MAYO || topping === Hamburger.ALL_TOPPINGS.TOPPING_SPICE) {
            this.getBurgerToppings().push(topping);
        } else {
            throw new HamburgerException("topping name is incorrect or not given");
        }
    } else {
        throw new HamburgerException("you already put this topping");
    }
};

Hamburger.prototype.removeTopping = function (topping) {
    if (topping === Hamburger.ALL_TOPPINGS.TOPPING_MAYO || topping === Hamburger.ALL_TOPPINGS.TOPPING_SPICE) {
        var filteredToppings = this.getBurgerToppings().filter(function (toppingType) {
            return toppingType !== topping;
        });
    } else {
        throw new HamburgerException("removing-topping name is incorrect or not given");
    }
    return this.setBurgerToppings(filteredToppings);
};

Hamburger.prototype.getToppings = function () {
    return this.getBurgerToppings();
};

Hamburger.prototype.getSize = function () {
    return this.getSize();
};

Hamburger.prototype.getStuffing = function () {
    return this.getStuffing();
};

Hamburger.prototype.calculatePrice = function () {
    return (this.getBurgerToppings().map(function(x){return x.price})).reduce(function(acc, prices) { return acc + prices}, 0)
        + this.getSize().price + this.getStuffing().price
};

Hamburger.prototype.calculateCallories = function () {
    return (this.getBurgerToppings().map(function(x){ return x.calories})).reduce( function(acc, prices) {return acc + prices}, 0)
        + this.getSize().calories + this.getStuffing().calories
};

function HamburgerException(message)  {
    this.name = "HamburgerException";
    this.message = message;

    if (Error.captureStackTrace) {
        Error.captureStackTrace(this, HamburgerException);
    } else {
        this.stack = (new Error()).stack;
    }
}

HamburgerException.prototype = Object.create(Error.prototype);


// var burgerRelise = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);

var burgerRelise = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

// console.log(burgerRelise.getSize());
// console.log(burgerRelise.getStuffing());
// burgerRelise.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO);
 burgerRelise.addTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE);
// console.log(burgerRelise.getToppings());
//
// console.log(burgerRelise.getToppings());
// console.log(burgerRelise.calculatePrice());
// console.log(burgerRelise.calculateCallories());
// console.log(burgerRelise.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_MAYO));
// console.log(burgerRelise.removeTopping(Hamburger.ALL_TOPPINGS.TOPPING_SPICE));
console.log(burgerRelise.calculatePrice());
console.log(burgerRelise.calculateCallories());